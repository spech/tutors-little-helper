# Tutors Little Helper

## Download
[Tutors Little Helper](https://gitlab.com/spech/tutors-little-helper/raw/master/EDV%20Zentrum%20Solver/bin/Release/Tutors%20Little%20Helper.exe)

Office Libs:  
[Excel](https://gitlab.com/spech/tutors-little-helper/raw/master/EDV%20Zentrum%20Solver/bin/Release/Microsoft.Office.Interop.Excel.dll)

[Word](https://gitlab.com/spech/tutors-little-helper/raw/master/EDV%20Zentrum%20Solver/bin/Release/Microsoft.Office.Interop.Word.dll)

[Office](https://gitlab.com/spech/tutors-little-helper/raw/master/EDV%20Zentrum%20Solver/bin/Release/Office.dll)

## Installation
Es ist keine Installation erforderlich. Einfach die EXE-Datei herunterladen und
ausführen.

Auf PCs mit Microsoft Office Versionen älter als 2013  muss man zusätzlich
die Microsoft Office Libraries herunterladen und die drei Dateien in dasselbe
Verzeichnis wie die EXE-Datei kopieren.
			
Getestete Office Versionen:
				
* Office 2013
* Office 2010 (Nur mit Libraries)
* Office 2007 (Nur mit Libraries)
	    		
## Anleitung
Einfach die EXE-Datei öffnen und das EXEL-, WORD oder LaTeX-Dokument auf das
Fenster ziehen. Das Programm kontrolliert die Datei und öffnet sie.
Unterstütze Dateiformate: *.doc, *.docx, *.xls, *.xlsx, *.dotx, *.tex Es können
auch mehrere Dateien unterschiedlichen Formates auf einmal ins Feld gezogen
werden.

Anmerkung zu LaTeX: Die Vorlage muss als Text in den Einstellungen eingetragen
werden.

## Was wird kontrolliert
Word:

* Zählt die Anzahl, der als Standard formatierten Absätze.
* Markiert die Standard Absätze rot.
* Zeigt den Autor der Datei an.
* Zählt die Bilder, welche nicht mit Abbildung oder Abbildungen formatiert sind.

Excel:

* Zähl die Zellen mit direkten Zellbezügen.
* Markiert diese Zellen rot.
* Zeigt den Autor der Datei an.
* Liest den Prefix der Namen aus und gibt eine Liste mit allen vorkommenden
  aus. (Wenn die Namenskonvention richtig verwendet wurde sollte nur ein
  Wert=Nachname ausgegeben werden)

LaTeX:

* Kontrolliert ob alle Befehle aus der Liste (Einstellungen) enthalten sind.
