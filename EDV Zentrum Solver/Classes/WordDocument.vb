﻿Imports Microsoft.Office.Interop

Public Class WordDocument
    Private path As String = ""
    Private fileName As String = ""
    Private oWord As Word.Application
    Private oDoc As Word.Document

    Public Sub New(ByVal path As String)
        Me.path = path
        Me.fileName = System.IO.Path.GetFileNameWithoutExtension(path)
    End Sub

    Public Sub load()
        Me.oWord = CreateObject("Word.Application")
        Me.oWord.Visible = False
        Me.oDoc = oWord.Documents.Open(Me.path)
    End Sub

    Public Sub run_Macro()
        Dim am_def_para As Integer
        Dim def_para_name As String
        Dim def_para_min_count As Integer
        Dim def_img_wrong As Integer

        am_def_para = 0 'Anzahl der gefundenen Absaetze
        def_img_wrong = 0

        def_para_name = "Standard" 'Nach dieser Formatvorlage wird gesucht
        def_para_min_count = 1 'Mindest Anzahl um als Absatz gewertet zu werden
        '''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''

        For i = 1 To oDoc.Paragraphs.Count
            Try
                Dim style_name As Word.Style = oDoc.Paragraphs(i).Range.Style
                If style_name.NameLocal = def_para_name And Len(oDoc.Paragraphs(i).Range.Text) - 1 > def_para_min_count Then
                    oDoc.Paragraphs(i).Range.HighlightColorIndex = Word.WdColorIndex.wdRed
                    am_def_para = am_def_para + 1
                End If
            Catch ex As Exception
            End Try
        Next i

        For i = 1 To oDoc.InlineShapes.Count
            Try
                Dim style_name As Word.Style = oDoc.InlineShapes(i).Range.ParagraphStyle
                If style_name.NameLocal <> "Abbildung" And style_name.NameLocal <> "Abbildungen" Then
                    def_img_wrong = def_img_wrong + 1
                End If
            Catch ex As Exception
            End Try
        Next

        Dim folderpath As String = Me.path.Substring(0, Me.path.Length - System.IO.Path.GetFileName(Me.path).Length)

        Me.fileName = Me.fileName.ToLower()
        Me.fileName = Me.fileName.Replace(oDoc.BuiltInDocumentProperties("Author").Value().ToString().ToLower(), "\b {" & oDoc.BuiltInDocumentProperties("Author").Value().ToString().ToLower() & "} \b0")

        If Form1.frmoptions.usebatchprocessing.Checked Then
            Konfiguration.append_to_file(Me.fileName & ";" & am_def_para & ";" & def_img_wrong & ";" & oDoc.BuiltInDocumentProperties("Author").Value() & "", Form1.frmoptions.batchDateiname.Text)
            Me.oDoc.Close(WdSaveOptions.wdDoNotSaveChanges)
            Konfiguration.write_to_output(Me.fileName)
        Else
            Konfiguration.write_to_output(Me.fileName & " \line " & "Anzahl der als " & def_para_name & " formatierten Absätze: \b{" & am_def_para & "\line" & "} \b0  Anzahl der falschen Bilder: \b {" & def_img_wrong & "} \b0 \line   Autor: \b{" & oDoc.BuiltInDocumentProperties("Author").Value() & "}\b0")
            Me.oWord.Visible = True
        End If

    End Sub

End Class
