﻿Imports Microsoft.Office.Interop
Imports System.Text.RegularExpressions 

Public Class ExcelDocument
    Private path As String = ""
    Private fileName As String = ""
    Private oExcel As Excel.Application
    Private oWBs As Excel.Workbooks
    Private oWB As Excel.Workbook

    Public Sub New(ByVal path As String)
        Me.path = path
        Me.fileName = System.IO.Path.GetFileNameWithoutExtension(path)
    End Sub

    Public Sub load()
        Me.oExcel = New Excel.Application
        Me.oExcel.Visible = False
        Me.oWBs = Me.oExcel.Workbooks
        Me.oWB = Me.oWBs.Open(Me.path)
    End Sub

    Public Sub run_Macro()
        Dim outputString As String = ""
        Dim anzDirekteBezuegeTotal As Integer = 0
        Dim anzDirekteBezuegeCell As Integer = 0

        Try
            Me.fileName = Me.fileName.ToLower()
            Me.fileName = Me.fileName.Replace(oWB.BuiltinDocumentProperties("Author").Value().ToString().ToLower(), "\b {" & oWB.BuiltinDocumentProperties("Author").Value().ToString().ToLower() & "} \b0")

        Catch ex As Exception

        End Try

        outputString = Me.fileName
        outputString &= "\line   Autor: \b{" & oWB.BuiltinDocumentProperties("Author").Value() & "}\b0"

        Dim distinct_usedPrefix As New ArrayList()
        Try
            '' Namenskonvention auslesen
            Dim usedPrefix As New ArrayList()
            For Each xlname As Excel.Name In oWB.Names
                usedPrefix.Add(xlname.Name.Split(".")(0))
            Next
            distinct_usedPrefix = Me.makeDistinc(usedPrefix)
            ' Namenskonvetion ausgeben
        Catch ex As Exception

        End Try
        outputString &= "\line   Namenskonvetion: \b{" & String.Join(", ", distinct_usedPrefix.ToArray()) & "}\b0"

        Try
            Dim xlWorksheets As Excel.Sheets = oWB.Sheets
            For Each xlWorksheet As Excel.Worksheet In xlWorksheets

                '' Anzahl der Direkten Zellbezüge auslesen
                Dim xlrange As Excel.Range = xlWorksheet.UsedRange
                For Each xlcell As Excel.Range In xlrange.Cells()
                    If xlcell.HasFormula Then
                        Dim formel As String = xlcell.FormulaR1C1
                        Dim regDirekt As Regex = New Regex("R(\[{0,1}-{0,1}\d{1,}\]{0,1}){0,1}C(\[{0,1}-{0,1}\d{1,}\]{0,1})")
                        If regDirekt.Match(formel).Success Then
                            xlcell.Interior.Color = Color.Red
                            anzDirekteBezuegeTotal += regDirekt.Matches(formel).Count
                            anzDirekteBezuegeCell += 1
                        End If
                    End If
                Next
            Next
        Catch ex As Exception
            anzDirekteBezuegeCell = -1
            anzDirekteBezuegeTotal = -1
        End Try

        'outputString &= "\line   Anzahl direkte Zellbezüge (Total): \b{" & anzDirekteBezuegeTotal & "}\b0"
        outputString &= "\line   Anzahl direkte Zellbezüge (Zellen): \b{" & anzDirekteBezuegeCell & "}\b0"

        If Form1.frmoptions.usebatchprocessing.Checked Then

            Konfiguration.append_to_file(Me.fileName & ";" & oWB.BuiltinDocumentProperties("Author").Value() & ";" & String.Join(", ", distinct_usedPrefix.ToArray()) & ";" & anzDirekteBezuegeCell, Form1.frmoptions.batchDateiname.Text)
            Me.oWB.Close(False)
            Konfiguration.write_to_output(Me.fileName)
        Else
            Konfiguration.write_to_output(outputString)
            Me.oExcel.Visible = True
        End If


    End Sub

    Private Function makeDistinc(ByVal fullArraylist As ArrayList) As ArrayList
        Dim distincArrayList As New ArrayList()
        For Each i As Object In fullArraylist
            If Not distincArrayList.Contains(i) Then
                distincArrayList.Add(i)
            End If
        Next
        Return distincArrayList
    End Function
End Class
