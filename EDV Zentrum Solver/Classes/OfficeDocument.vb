﻿Public Class OfficeDocument
    Private path As String = ""
    Private filename As String = ""
    Private extension As String = ""
    Private new_filename As String = ""

    Public Sub New(ByVal path As String)
        Me.path = path
        Me.extension = System.IO.Path.GetExtension(Me.path)
        Me.filename = System.IO.Path.GetFileNameWithoutExtension(Me.path)
    End Sub

    Public Sub solve_OfficeDocument()
        Try
            Select Case Me.extension
                Case ".docx"
                    solve_word()
                Case ".doc"
                    solve_word()
                Case ".dotx"
                    solve_word()
                Case ".xls"
                    solve_excel()
                Case ".xlsx"
                    solve_excel()
                Case ".tex"
                    solve_latex()
            End Select
        Catch ex As Exception
            Konfiguration.write_to_output(ex.InnerException().ToString())
        End Try

    End Sub

    Private Sub solve_word()
        Dim tempDocument As New WordDocument(Me.path)
        tempDocument.load()
        tempDocument.run_Macro()
    End Sub

    Private Sub solve_excel()
        Dim tempDocument As New ExcelDocument(Me.path)
        tempDocument.load()
        tempDocument.run_Macro()
    End Sub

    Private Sub solve_latex()
        Dim tempDocument As New LaTeXDocument(Me.path)
        tempDocument.load()
        tempDocument.parse_Document()
    End Sub
End Class
