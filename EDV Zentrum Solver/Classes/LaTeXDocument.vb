﻿Imports System.Text.RegularExpressions

Public Class LaTeXDocument
    Private path As String = ""
    Private fileName As String = ""

    Private file_content As String = ""

    Private file As System.IO.StreamReader

    Public Sub New(ByVal path As String)
        Me.path = path
        Me.fileName = System.IO.Path.GetFileNameWithoutExtension(path)
    End Sub

    Public Sub load()
        Me.file_content = My.Computer.FileSystem.ReadAllText(path)
    End Sub

    Public Sub parse_Document()
        Dim output As String = Me.fileName & "\line"
        Dim Lines As String() = Form1.frmoptions.latexVorlage.Text.Split(vbCrLf)
        Dim firstRun As Boolean = True
        Dim everythingCorr As Boolean = True
        Dim countWrong As Integer = 0
        Dim basisAlt As Integer = 0
        For Each line As String In Lines
            'In case there is a double semicolon in the line split it there
            Dim linesp() As String = line.Split(New String() {";;"}, StringSplitOptions.None)

            Dim lineSearch As String = linesp(0)
            Dim lineCount As Integer = -1
            If linesp.Length = 2 Then
                lineCount = linesp(1)
                If lineCount <> -1 Then
                    basisAlt = basisAlt + lineCount
                End If
            End If

            If Regex.Matches(Me.file_content, lineSearch, RegexOptions.IgnorePatternWhitespace).Count <> lineCount Then
                output = output & " \b{" & Konfiguration.mask_String(lineSearch) & " (Anz: " & Regex.Matches(Me.file_content, lineSearch, RegexOptions.IgnorePatternWhitespace).Count & "/" & lineCount & ")" & "} \b0 \line"
                everythingCorr = False
                If lineCount <> -1 Then
                    countWrong = countWrong + Math.Abs(Regex.Matches(Me.file_content, lineSearch, RegexOptions.IgnorePatternWhitespace).Count - lineCount)
                End If
            ElseIf Form1.frmoptions.cbDetails.Checked Then
                output = output & " " & Konfiguration.mask_String(lineSearch) & " (Anz: " & Regex.Matches(file_content, lineSearch, RegexOptions.IgnorePatternWhitespace).Count & "/" & lineCount & ")" & "\line"
            End If

        Next

        If everythingCorr Then
            output = output & " Alles richtig" & " \line"
        Else
            output = output & " In summe sind " & countWrong & " Befehle fehlerhaft" & " \line"
            Dim punkteBasis As Double = Form1.frmoptions.tbBasis.Text
            output = output & " " & (basisAlt - countWrong) & " von " & (basisAlt) & " Befehlen richtig verwendet." & " \line"
            output = output & " Umbasiert auf " & punkteBasis & " ergibt das " & Math.Ceiling((basisAlt - countWrong) / basisAlt * punkteBasis) & " Punkte " & " \line"

        End If

        Konfiguration.write_to_output(output)
        Process.Start(path)
    End Sub

End Class
