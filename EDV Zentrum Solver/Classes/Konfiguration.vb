﻿Imports System.IO
Imports System.Text

Public Class Konfiguration
    'Moegliche Extensions mit . angeben
    Public Shared extenions As String() = {".xls", ".xlsx", ".doc", ".docx", ".dotx", ".tex"}

    Public Shared Sub write_to_output(ByVal text As String)
        Dim timeStamp As String = Now.ToShortTimeString
        Form1.tb_output.Rtf = ("{\rtf1\ansi [" & timeStamp & "]: " & text & " \line ") & Form1.tb_output.Rtf & "}"
    End Sub

    Public Shared Sub write_to_output_no_time(ByVal text As String)
        Dim timeStamp As String = Now.ToShortTimeString
        Form1.tb_output.AppendText(text & vbNewLine)
    End Sub

    Public Shared Sub write_to_file(ByVal text As String, ByVal path As String)

        ' Create or overwrite the file. 
        Dim fs As FileStream = File.Create(path)

        ' Add text to the file. 
        Dim info As Byte() = New UTF8Encoding(True).GetBytes(text)
        fs.Write(info, 0, info.Length)
        fs.Close()
    End Sub

    Public Shared Sub append_to_file(ByVal text As String, ByVal path As String)

        Dim objWriter As New System.IO.StreamWriter(path, True)
        objWriter.WriteLine(text)

        objWriter.Close()
    End Sub

    Public Shared Function mask_String(str)
        Dim output As String = Replace(str, "\", "\\")
        output = Replace(output, "{", "\{")
        output = Replace(output, "}", "\}")
        Return output
    End Function
End Class
