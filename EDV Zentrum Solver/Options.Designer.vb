﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Options
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Options))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.batchDateiname = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.usebatchprocessing = New System.Windows.Forms.CheckBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.tbBasis = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbDetails = New System.Windows.Forms.CheckBox()
        Me.latexVorlage = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Margin = New System.Windows.Forms.Padding(6)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(836, 873)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 34)
        Me.TabPage2.Margin = New System.Windows.Forms.Padding(6)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(6)
        Me.TabPage2.Size = New System.Drawing.Size(828, 835)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Allgemein"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.batchDateiname)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.usebatchprocessing)
        Me.GroupBox1.Location = New System.Drawing.Point(18, 13)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox1.Size = New System.Drawing.Size(786, 225)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Batch Processing"
        '
        'batchDateiname
        '
        Me.batchDateiname.Location = New System.Drawing.Point(142, 160)
        Me.batchDateiname.Margin = New System.Windows.Forms.Padding(6)
        Me.batchDateiname.Name = "batchDateiname"
        Me.batchDateiname.Size = New System.Drawing.Size(624, 31)
        Me.batchDateiname.TabIndex = 4
        Me.batchDateiname.Text = "bauinfo_auswertung.csv"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 165)
        Me.Label2.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(115, 25)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Dateiname"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 38)
        Me.Label1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(768, 75)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = resources.GetString("Label1.Text")
        '
        'usebatchprocessing
        '
        Me.usebatchprocessing.AutoSize = True
        Me.usebatchprocessing.Location = New System.Drawing.Point(20, 119)
        Me.usebatchprocessing.Margin = New System.Windows.Forms.Padding(6)
        Me.usebatchprocessing.Name = "usebatchprocessing"
        Me.usebatchprocessing.Size = New System.Drawing.Size(139, 29)
        Me.usebatchprocessing.TabIndex = 1
        Me.usebatchprocessing.Text = "Aktivieren"
        Me.usebatchprocessing.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.tbBasis)
        Me.TabPage3.Controls.Add(Me.Label4)
        Me.TabPage3.Controls.Add(Me.cbDetails)
        Me.TabPage3.Controls.Add(Me.latexVorlage)
        Me.TabPage3.Controls.Add(Me.Label3)
        Me.TabPage3.Location = New System.Drawing.Point(4, 34)
        Me.TabPage3.Margin = New System.Windows.Forms.Padding(6)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(828, 835)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "LaTeX Vorlage"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'tbBasis
        '
        Me.tbBasis.Location = New System.Drawing.Point(445, 157)
        Me.tbBasis.Name = "tbBasis"
        Me.tbBasis.Size = New System.Drawing.Size(123, 31)
        Me.tbBasis.TabIndex = 4
        Me.tbBasis.Text = "30"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(277, 159)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(158, 25)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Basis (Punkte):"
        '
        'cbDetails
        '
        Me.cbDetails.AutoSize = True
        Me.cbDetails.Location = New System.Drawing.Point(22, 158)
        Me.cbDetails.Name = "cbDetails"
        Me.cbDetails.Size = New System.Drawing.Size(232, 29)
        Me.cbDetails.TabIndex = 2
        Me.cbDetails.Text = "Detailierte Ausgabe"
        Me.cbDetails.UseVisualStyleBackColor = True
        '
        'latexVorlage
        '
        Me.latexVorlage.Location = New System.Drawing.Point(22, 213)
        Me.latexVorlage.Margin = New System.Windows.Forms.Padding(6)
        Me.latexVorlage.Multiline = True
        Me.latexVorlage.Name = "latexVorlage"
        Me.latexVorlage.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.latexVorlage.Size = New System.Drawing.Size(770, 591)
        Me.latexVorlage.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 17)
        Me.Label3.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(793, 125)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "LaTeX Vorlage Datei einfügen. Bei jeder Zeile wird kontrolliert, ob sie in der Da" &
    "tei " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "enthalten ist." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Beispiel: \\usepackage\[utf8\]{inputenc}" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Abstand mit \s" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) &
    "Beispiel:  \\subsection{Aufgabe\s02}"
        '
        'Options
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(836, 873)
        Me.ControlBox = False
        Me.Controls.Add(Me.TabControl1)
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.Name = "Options"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Einstellungen"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents usebatchprocessing As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents batchDateiname As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents latexVorlage As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbDetails As System.Windows.Forms.CheckBox
    Friend WithEvents tbBasis As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
