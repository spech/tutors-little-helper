﻿Imports System.IO

Public Class Form1
    Private defaultText As String = ""
    Public frmoptions As Options

    Private Sub tb_output_DragDrop1(sender As Object, e As DragEventArgs) Handles Me.DragDrop
        Label1.Text = "Working"
        Dim files() As String = e.Data.GetData(DataFormats.FileDrop)

        If frmoptions.usebatchprocessing.Checked = False And files.Count > 5 Then
            Dim result As DialogResult = MessageBox.Show("Hast du vergessen Batch Processing zu aktivieren?", "Achtung", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
            If result = System.Windows.Forms.DialogResult.Yes Then
                frmoptions.usebatchprocessing.Checked = True
            End If
        End If

        If frmoptions.usebatchprocessing.Checked Then
            Konfiguration.write_to_file("Batch Processing " & DateTime.Now.ToString("de-DE") & vbCrLf, frmoptions.batchDateiname.Text)
        End If
        For Each path As String In files
            If File.Exists(path) Then
                Dim doc As New OfficeDocument(path)
                doc.solve_OfficeDocument()
            End If
        Next
        Me.BringToFront()
        Label1.Text = Me.defaultText
    End Sub


    Private Sub Form1_DragEnter(sender As Object, e As DragEventArgs) Handles Me.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.Copy
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        tb_output.AllowDrop = True
        Me.defaultText = Label1.Text
        Me.frmoptions = New Options()

    End Sub

    Private Sub ToolStripLabel1_Click(sender As Object, e As EventArgs) Handles ToolStripLabel1.Click
        Dim about As New AboutBox()
        about.ShowDialog()

    End Sub

    Private Sub ToolStripLabel2_Click(sender As Object, e As EventArgs) Handles ToolStripLabel2.Click
        If Me.frmoptions.Visible Then
            Me.frmoptions.Hide()
            Me.ToolStripLabel2.Text = "Zeige Einstellungen"
        Else
            Me.frmoptions.Show()
            Me.ToolStripLabel2.Text = "Verstecke Einstellungen"
        End If
    End Sub

End Class
